#!/bin/bash

cd $ROS_WORKSPACE

# install msf
git clone https://github.com/ethz-asl/asctec_mav_framework
git clone https://github.com/ethz-asl/catkin_simple
git clone https://github.com/ethz-asl/gflags_catkin.git
git clone https://github.com/ethz-asl/glog_catkin
git clone https://github.com/richardw347/ethzasl_msf

# install hector
git clone -b indigo-devel https://github.com/tu-darmstadt-ros-pkg/hector_quadrotor.git
git clone -b indigo-devel https://github.com/tu-darmstadt-ros-pkg/hector_gazebo.git

# install dctam
git clone https://github.com/richardw347/dctam_controller.git
git clone -b demo-devel https://github.com/richardw347/dctam_tracker.git
git clone https://github.com/richardw347/dctam_mapper.git
git clone https://github.com/richardw347/dctam_msgs.git
git clone https://richardw347@bitbucket.org/richardw347/dctam_scalability.git
git clone https://richardw347@bitbucket.org/richardw347/drone_gazebo.git


