#!/usr/bin/env python
import rospkg
import datetime
import math
import numpy as np
import tf.transformations
from nav_msgs.msg import Odometry
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d
import os

agent_data = []


def open_file_and_process(directory, file):
    print("Processing file: %s" % file)
    date_str = file.strip("position_error_data_").strip(".txt")
    f = open(directory + file, 'r')
    lines = f.readlines()
    time = []

    est_x = []
    est_y = []
    est_z = []

    tru_x = []
    tru_y = []
    tru_z = []

    first_time = 0.0
    first = True
    # print len(lines)
    for line in lines:
        data = line.split(";")
        if first:
            first_time = float(data[0])
            first = False
        time.append(float(data[0]) - first_time)
        est_x.append(float(data[1]))
        est_y.append(float(data[2]))
        est_z.append(float(data[3]))
        tru_x.append(float(data[4]))
        tru_y.append(float(data[5]))
        tru_z.append(float(data[6]))

    x_scale = 1.0
    y_scale = 1.0
    z_scale = 1.18
    # z_scale = compute_scale_factor(est_z, tru_z)
    #
    # rospy.loginfo("Scale factors set")
    # rospy.loginfo("Values are x: %f y: %f z: %f"%(x_scale, y_scale, z_scale))
    #
    for idx in range(len(est_x)):
        est_x[idx] *= x_scale
        est_y[idx] *= y_scale
        #est_y[idx] -= 0.5
        #est_y[idx] -= 0.7
        est_x[idx] -= 0.18
        est_z[idx] *= z_scale

    agent_data.append([est_x, est_y, est_z, tru_x, tru_y, tru_z, time])


def compute_rmse_over_time(est_x, est_y, est_z, tru_x, tru_y, tru_z):
    rmse_all = []
    prev_rmse = 0.0
    first = True
    for idx in range(len(est_x)):
        error_x_sq = math.pow((tru_x[idx] - est_x[idx]), 2)
        error_y_sq = math.pow((tru_y[idx] - est_y[idx]), 2)
        error_z_sq = math.pow((tru_z[idx] - est_z[idx]), 2)
        total_error_sq = (error_x_sq + error_y_sq + error_z_sq) / 3.0
        rmse = math.sqrt(total_error_sq)
        if first:
            rmse_all.append(rmse)
            first = False
        else:
            rmse = (0.2 * rmse) + (0.8 * prev_rmse)
            rmse_all.append(rmse)
    return rmse_all


if __name__ == '__main__':
    r = rospkg.RosPack()
    package_path = r.get_path("dctam_scalability")
    data_path_pose = package_path + "/data/"
    files = os.listdir(data_path_pose)
    date_str = ''
    for fl in files:
        if fl.startswith("position_error_data_"):
            open_file_and_process(data_path_pose, fl)
            date_str = fl.strip("position_error_data_").strip(".txt")
    #colors = ['BlueViolet', 'Chocolate', 'Black', 'Crimson', 'DarkGoldenRod', 'DarkRed', 'HotPink']
    colors = ['DarkRed'] * 25
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    gt = False
    for idx, agent in enumerate(agent_data):

        if idx == (len(agent_data) - 1):
            ax.plot_wireframe(agent[3], agent[4], agent[5], color="Green", label="Ground Truth", linewidth=2.0, linestyle="--")
            ax.plot_wireframe(agent[0], agent[1], agent[2], color=colors[idx], label="MAV", linewidth=2.0)
        else:
            ax.plot_wireframe(agent[3], agent[4], agent[5], color="Green", linewidth=2.0, linestyle="--")
            ax.plot_wireframe(agent[0], agent[1], agent[2], color=colors[idx], linewidth=2.0)

    ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05),
              fancybox=True, shadow=True, ncol=4)
    ax.set_xlabel('X Position in Metres')
    ax.set_ylabel('Y Position in Metres')
    ax.set_zlabel('Z Position in Metres')
    ax.set_zlim(0.0, 3.0)
    ax.set_xlim(-8.1, 8.1)
    ax.set_ylim(-8.1, 8.1)
    file = data_path_pose + "position_error_plot_combined_" + date_str + "_3d.png"
    plt.savefig(file, bbox_inches='tight')

    fig = plt.figure()  # plt.figure(figsize=(10, 5), dpi=100)
    for idx, agent in enumerate(agent_data):

        if idx == (len(agent_data) - 1):
            plt.plot(agent[3], agent[4], color="Green", label="Ground Truth", linewidth=2.0, linestyle="--")
            plt.plot(agent[0], agent[1], color=colors[idx], label="MAV", linewidth=2.0)
            gt = True
        else:
            plt.plot(agent[0], agent[1], color=colors[idx], linewidth=2.0)
            plt.plot(agent[3], agent[4], color="Green", linewidth=2.0, linestyle="--")

    plt.xlabel('X Position in Metres')
    plt.ylabel('Y Position in Metres')
    plt.xlim(-8.1, 8.1)
    plt.ylim(-8.1, 8.1)
    plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.12),
               fancybox=True, shadow=True, ncol=4)
    file = data_path_pose + "position_error_plot_combined_" + date_str + ".png"
    plt.savefig(file, bbox_inches='tight')
