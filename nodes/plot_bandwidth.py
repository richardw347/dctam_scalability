#!/usr/bin/env python
import rospkg
import datetime
import math
import numpy as np
import tf.transformations
from nav_msgs.msg import Odometry
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d
import os

agent_bw_sent_data = []
agent_bw_recd_data = []


def open_file_and_process(directory, file):
    print("Processing file: %s" % file)
    f = open(directory + file, 'r')
    lines = f.readlines()
    time = []
    sent_bw = []

    first_time = 0.0
    first = True
    # print len(lines)
    total = 0
    last_time = 0
    for line in lines:
        data = line.split(";")
        if first:
            first_time = float(data[0])
            first = False

        t = (float(data[0]) - first_time) / 1000000000.0
        bw = float(data[1]) / 1000.0
        time.append(t)
        sent_bw.append(bw)
        total += bw
        last_time = t

    agent_bw_sent_data.append([time, 0, sent_bw, total, total/last_time])


def load_recd_data(directory, file):
    print("Processing file: %s" % file)
    f = open(directory + file, 'r')
    lines = f.readlines()
    time = []
    recd_bw = []

    first_time = 0.0
    first = True
    # print len(lines)
    total = 0
    last_time = 0
    for line in lines:
        data = line.split(";")
        if first:
            first_time = float(data[0])
            first = False

        t = (float(data[0]) - first_time) / 1000000000.0
        bw = float(data[1]) / 1000.0
        time.append(t)
        recd_bw.append(bw)
        total += bw
        last_time = t

    agent_bw_recd_data.append([time, 0, recd_bw, total, total/last_time])


if __name__ == '__main__':
    r = rospkg.RosPack()
    package_path = r.get_path("dctam_scalability")
    data_path_pose = package_path + "/data/"
    files = os.listdir(data_path_pose)
    date_str = ''
    got_recd = False
    for fl in files:
        if fl.startswith("bandwidth_sent_data_"):
            open_file_and_process(data_path_pose, fl)
            date_str = fl.strip("bandwidth_sent_data_").strip(".txt")
        if not got_recd and fl.startswith("bandwidth_recv_data_"):
            load_recd_data(data_path_pose, fl)
            got_recd = True

    colors = ['DarkRed'] * 25
    fig = plt.figure(figsize=(12, 6))
    sent_bw = 0
    sent_total = 0
    for idx, agent in enumerate(agent_bw_sent_data):
        if idx == (len(agent_bw_sent_data) - 1):
            plt.vlines(agent[0], agent[1], agent[2], linestyles=['solid'], colors=['orangered'], linewidth=2.5, label="Sent Messages")
        else:
            plt.vlines(agent[0], agent[1], agent[2], linestyles=['solid'], colors=['orangered'], linewidth=2.5)
        print ("SENT: total messages %f, bandwidth: %f" %(agent[3], agent[4]))
        sent_bw += agent[4]
        sent_total += agent[3]
    print ("OVERALL SENT: total messages %f, bandwidth: %f" %(sent_total, sent_bw))
    recv = agent_bw_recd_data[0]
    plt.vlines(recv[0], recv[1], recv[2], linestyles=['--'], colors=['royalblue'], linewidth=2.5, label="Received Messages")
    print ("RECD: total messages %f, bandwidth: %f" %(recv[3], recv[4]))
    plt.xlabel('Time in Seconds (s)')
    plt.ylabel('Message Size in Kilobytes (Kb)')
    plt.legend()
    file = data_path_pose + "bandwidth_plot_" + date_str + ".png"
    plt.savefig(file, bbox_inches='tight')
