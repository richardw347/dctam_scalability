#!/usr/bin/env python

import rospy
from geometry_msgs.msg import PoseArray, PoseStamped
from nav_msgs.msg import Odometry
from dctam_msgs.msg import MoveDroneAction, MoveDroneResult, MoveDroneGoal
from dctam_msgs.srv import GetPoints, GetPointsRequest, GetPointsResponse
from sensor_msgs.msg import PointCloud
import actionlib
from dctam_msgs.msg import DCTAMTrackerState
from geometry_msgs.msg import Twist
from gazebo_msgs.srv import SetModelState, SetModelStateRequest, SetModelStateResponse
from dynamic_reconfigure.msg import BoolParameter
from dynamic_reconfigure.srv import Reconfigure, ReconfigureRequest, ReconfigureResponse
from sensor_fusion_comm.srv import InitHeight, InitHeightRequest
from nav_msgs.msg import Path
import numpy as np
import subprocess
import psutil
import math
import sys
import copy
from random import randint
import matplotlib.pyplot as plt
from experiment_agent import Agent

NUM_AGENTS = 4
INIT_HEIGHT = 3.2
HEIGHT = 2.5
RADIUS = 2.4
GOAL_RADIUS = 10.0
GROUND_TRUTH_CONTROL = True
# 0 1 2 3 4
HEXTANT = 4


class Experiment:
    def __init__(self):
        rospy.init_node("experiment")
        self.agents = []
        for i in range(NUM_AGENTS):
            agent_num = i + (HEXTANT * NUM_AGENTS)
            self.agents.append("agent" + str(agent_num))
        self.agent_clients = []
        self.agent_procs = []
        self.colors = ['BlueViolet', 'Chocolate', 'Black', 'Crimson', 'DarkGoldenRod', 'DarkRed', 'HotPink', 'LawnGreen']

        proc = subprocess.Popen(['roslaunch drone_description crazy.launch'], shell=True)
        self.agent_procs.append(proc)
        rospy.sleep(10.0)

        anuglar_range = math.pi * 2
        for i, agent in enumerate(self.agents):
            rospy.loginfo("spawning robot: " + str(agent))

            agent_num = i + (HEXTANT * NUM_AGENTS)
            x = math.cos(agent_num * anuglar_range / (NUM_AGENTS * 5)) * RADIUS
            y = math.sin(agent_num * anuglar_range / (NUM_AGENTS * 5)) * RADIUS

            goal_x = math.cos(agent_num * anuglar_range / (NUM_AGENTS * 5)) * GOAL_RADIUS
            goal_y = math.sin(agent_num * anuglar_range / (NUM_AGENTS * 5)) * GOAL_RADIUS

            goal_x1 = math.cos((agent_num+1) * anuglar_range / (NUM_AGENTS * 5)) * GOAL_RADIUS
            goal_y1 = math.sin((agent_num+1) * anuglar_range / (NUM_AGENTS * 5)) * GOAL_RADIUS

            proc = subprocess.Popen(['roslaunch dctam_scalability agent.launch id:=' + str(agent_num) + ' name:=' +
                                     str(agent) + ' x:=' + str(x) + ' y:=' + str(y) + ' ground_truth_control:=' +
                                     str(GROUND_TRUTH_CONTROL)], shell=True)
            self.agent_procs.append(proc)
            agent = Agent(agent, [x, y, INIT_HEIGHT], [goal_x, goal_y, HEIGHT])
            agent.add_goal(x, y, HEIGHT-0.5)
            agent.add_goal(goal_x, goal_y, HEIGHT)
            agent.add_goal(goal_x1, goal_y1, HEIGHT)
            self.agent_clients.append(agent)
            rospy.sleep(2.0)

        if not GROUND_TRUTH_CONTROL:
            proc_mp = subprocess.Popen(['roslaunch dctam_experiments mapper.launch'], shell=True)
            self.agent_procs.append(proc_mp)
            rospy.sleep(10.0)


    def clean_up(self):
        rospy.loginfo("cleaning up")
        for proc in self.agent_procs:
            self.term(proc.pid)

    def term(self, proc_pid):
        process = psutil.Process(proc_pid)
        for proc in process.get_children(recursive=True):
            proc.terminate()
        process.terminate()

    def init(self):
        rospy.sleep(10.0)
        rospy.loginfo("takeoff")
        for client in self.agent_clients:
            client.take_off()

        if GROUND_TRUTH_CONTROL:
            return

        rospy.sleep(2.0)

        rospy.loginfo("teleport")
        for client in self.agent_clients:
            client.teleport()

        rospy.sleep(1.0)

        rospy.loginfo("initializing ptam")
        for client in self.agent_clients:
            client.do_init_ptam()

    def spin(self):
        rospy.loginfo("running tests")
        for client in self.agent_clients:
            client.goto_start()

        for client in self.agent_clients:
            client.wait_for_result()

        rospy.sleep(5.0)

        for client in self.agent_clients:
            client.goto_goals()

        for client in self.agent_clients:
            client.wait_for_result()


if __name__ == '__main__':
    act = Experiment()
    act.init()
    act.spin()
    rospy.spin()
    act.clean_up()
