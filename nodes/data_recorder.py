#!/usr/bin/env python
import datetime
import rospkg
import rospy
from nav_msgs.msg import Odometry
from dctam_msgs.msg import MapUpdate, BundleUpdate, NewKeyframe, KeyFrame, Measurement, Camera, MapPoint, SourceKF
from geometry_msgs.msg import PoseWithCovarianceStamped

# new keyframe message sizes in bytes
KEYFRAME_SIZE = 116
MEASUREMENT_SIZE = 65

# map update message sizes in bytes
MAPPOINT_SIZE = 176
MAP_UPDATE_MISC_SIZE = 17

# bundle update message sizes in bytes
KEYFRAME_UPDATE_SIZE = 52
MAPPOINT_UPDATE_SIZE = 28
POINT_DELETE_SIZE = 4

NUM_AGENTS = 20


class Agent:
    def __init__(self, agent):
        self.name = agent
        self.state_sub = rospy.Subscriber(str(agent) + "/ground_truth/state", Odometry, self.state_cb)
        self.ptam_sub = rospy.Subscriber(str(agent) + "/pose", PoseWithCovarianceStamped, self.ptam_cb)
        self.truth_x = 0.0
        self.truth_y = 0.0
        self.truth_z = 0.0
        self.poses = []

    def state_cb(self, msg):
        assert(isinstance(msg, Odometry))
        self.truth_x = msg.pose.pose.position.x
        self.truth_y = msg.pose.pose.position.y
        self.truth_z = msg.pose.pose.position.z

    def ptam_cb(self, msg):
        assert(isinstance(msg, PoseWithCovarianceStamped))
        x = msg.pose.pose.position.x
        y = msg.pose.pose.position.y
        z = msg.pose.pose.position.z
        self.poses.append([rospy.Time.now().to_sec(), x, y, z, self.truth_x, self.truth_y, self.truth_z])


class Global:
    def __init__(self, filepath):
        rospy.init_node("data_recorder")
        self.map_update_sub = rospy.Subscriber("/tracker/map_update", MapUpdate, self.map_update_cb)
        self.bundle_update_sub = rospy.Subscriber("/tracker/bundle_update", BundleUpdate, self.bundle_update_cb)
        self.new_keyframe_sub = rospy.Subscriber("/tracker/new_keyframe", NewKeyframe, self.new_keyframe_cb)
        self.map_update_bandwidth = []
        self.bundle_update_bandwidth = []
        self.new_keyframe_bandwidth = []
        self.agents = []
        for i in range(NUM_AGENTS):
            self.agents.append(Agent("agent" + str(i)))
        rospy.loginfo("collecting data")
        rospy.spin()
        rospy.loginfo("building data file")
        file = open(filepath, mode='w')

        file.write("global_data:map_update_bandwidth\n")
        file.write("time,size\n")
        for mu in self.map_update_bandwidth:
            file.write("%d,%d\n"%(mu[0], mu[1]))

        file.write("global_data:bundle_update_bandwidth\n")
        file.write("time,size\n")
        for mu in self.bundle_update_bandwidth:
            file.write("%d,%d\n"%(mu[0], mu[1]))

        file.write("global_data:new_keyframe_bandwidth\n")
        file.write("time,agent,size\n")
        for mu in self.new_keyframe_bandwidth:
            file.write("%d,%s,%d\n"%(mu[0], mu[1], mu[2]))

        for agent in self.agents:
            assert(isinstance(agent, Agent))
            file.write("agent_position_data:%s\n" %(agent.name))
            file.write("time,est_x,est_y,est_z,tru_x,tru_y,tru_z\n")
            for pos in agent.poses:
                file.write("%d,%f,%f,%f,%f,%f,%f\n" %(pos[0], pos[1], pos[2], pos[3], pos[4], pos[5], pos[6]))
        rospy.loginfo("data file saved to: %s" %(filepath))
        file.close()


    def map_update_cb(self, msg):
        assert(isinstance(msg, MapUpdate))
        size = len(msg.new_points) * MAPPOINT_SIZE + MAP_UPDATE_MISC_SIZE
        for kf in msg.frames:
            assert (isinstance(kf, KeyFrame))
            size += len(kf.compressed_keyframe_image.data)
            size += KEYFRAME_SIZE
        self.map_update_bandwidth.append([rospy.Time.now().to_sec(), size])

    def bundle_update_cb(self, msg):
        assert(isinstance(msg, BundleUpdate))
        size = len(msg.kf_updates) * KEYFRAME_UPDATE_SIZE + len(msg.mp_updates) * MAPPOINT_UPDATE_SIZE + \
               len(msg.points_to_delete) * POINT_DELETE_SIZE
        self.bundle_update_bandwidth.append([rospy.Time.now().to_sec(), size])

    def new_keyframe_cb(self, msg):
        assert(isinstance(msg, NewKeyframe))
        size = len(msg.frame.compressed_keyframe_image.data)  + KEYFRAME_SIZE + len(msg.meas) * MEASUREMENT_SIZE
        name = msg.frame.cam.cam_name
        self.new_keyframe_bandwidth.append([rospy.Time.now().to_sec(), name, size])


if __name__ == '__main__':
    rospy.init_node("data_recorder")
    rospack = rospkg.RosPack()
    pack_path = rospack.get_path('dctam_scalability')
    current_time = datetime.datetime.now()
    current_time.day
    filename = "scalability_expt_%d_%d_%d_%d_%d.txt" %(current_time.day, current_time.month, current_time.year, current_time.hour, current_time.minute)
    file_path = pack_path + "/experiment_data/" + filename
    node = Global(file_path)