#!/usr/bin/env python

import rospy
from geometry_msgs.msg import PoseArray, PoseStamped
from nav_msgs.msg import Odometry
from dctam_msgs.msg import MoveDroneAction, MoveDroneResult, MoveDroneGoal
from dctam_msgs.srv import GetPoints, GetPointsRequest, GetPointsResponse
from sensor_msgs.msg import PointCloud
import actionlib
from dctam_msgs.msg import DCTAMTrackerState
from geometry_msgs.msg import Twist, Pose
from gazebo_msgs.srv import SetModelState, SetModelStateRequest, SetModelStateResponse
from dynamic_reconfigure.msg import BoolParameter
from dynamic_reconfigure.srv import Reconfigure, ReconfigureRequest, ReconfigureResponse
from sensor_fusion_comm.srv import InitHeight, InitHeightRequest
from nav_msgs.msg import Path
from std_msgs.msg import Empty
import numpy as np
import subprocess
import psutil
import math
import sys
import copy


class Agent:
    def __init__(self, agent, start_pose, goal_pose):
        self.name = agent
        self.client = actionlib.SimpleActionClient(str(agent) + '/move_drone', MoveDroneAction)
        self.start_pose = start_pose
        self.goal_pose = goal_pose
        self.state_sub = rospy.Subscriber(str(agent) + "/ground_truth/state", Odometry, self.state_cb)
        self.filter_sub = rospy.Subscriber(str(agent) + "/msf_core/odometry", Odometry, self.filter_state_cb)
        self.ptam_state_sub = rospy.Subscriber(str(agent) + "/state", DCTAMTrackerState, self.ptam_state_cb)
        self.vel_pub = rospy.Publisher(str(agent) + "/cmd_vel", Twist, queue_size=5)
        self.teleport_sub = rospy.Subscriber(str(agent) + "teleport", Pose, self.teleport_cb)
        self.goals = []
        self.x = []
        self.y = []
        self.ptam_state = False
        self.tracking_state = False
        self.init = False
        self.truth_z = 0.0
        self.frontier_points_won = Path()
        self.current_x = 0.0
        self.current_y = 0.0
        self.current_z = 0.0

    def teleport_cb(self, msg):
        self.teleport_to(msg.position.x, msg.position.y, msg.position.z)

    def take_off(self):
        t = Twist()
        t.linear.z = 0.5
        self.vel_pub.publish(t)
        rospy.sleep(1.0)
        t.linear.z = 0.0
        self.vel_pub.publish(t)
        rospy.sleep(1.0)

    def wiggle(self, move, length, dir=0):
        t = Twist()
        if dir == 0:
            t.linear.x = move
        elif dir == 1:
            t.linear.y = move
        else:
            t.linear.z = move
        self.vel_pub.publish(t)
        rospy.sleep(length)
        t.linear.x = 0.0
        t.linear.y = 0.0
        t.linear.z = 0.0
        self.vel_pub.publish(t)
        rospy.sleep(1.0)

    def teleport_start(self):
        self.teleport_to(self.start_pose[0], self.start_pose[1], self.start_pose[2])

    def teleport_to(self, x, y, z):
        rospy.wait_for_service("/gazebo/set_model_state")
        set_model = rospy.ServiceProxy("/gazebo/set_model_state", SetModelState)
        try:
            modst = SetModelStateRequest()
            modst.model_state.model_name = self.name
            modst.model_state.pose.position.x = x
            modst.model_state.pose.position.y = y
            modst.model_state.pose.position.z = z
            set_model(modst)
        except:
            rospy.logerr("error setting model state" + str(sys.exc_info()[0]))

    def ptam_state_cb(self, msg):
        self.ptam_state = msg.mapOK
        if msg.trackingQuality == 2:
            self.tracking_state = True
        else:
            self.tracking_state = False

    def state_cb(self, msg):
        self.x.append(msg.pose.pose.position.x)
        self.y.append(msg.pose.pose.position.y)
        self.truth_z = msg.pose.pose.position.z

    def filter_state_cb(self, msg):
        self.current_x = msg.pose.pose.position.x
        self.current_y = msg.pose.pose.position.y
        self.current_z = msg.pose.pose.position.z

    def do_init_ptam(self):
        while not self.ptam_state and not rospy.is_shutdown():
            rospy.loginfo(self.name + " waiting for localisation")
            rospy.sleep(0.2)

        while not self.tracking_state and not rospy.is_shutdown():
            rospy.loginfo(self.name + "attemping to localise")
            self.wiggle(0.7, 2.0, 2)  # zup
            if self.tracking_state:
                break
            self.wiggle(0.7, 2.0, 0)  # forward
            if self.tracking_state:
                break
            self.wiggle(-0.7, 2.0, 0)  # back
            if self.tracking_state:
                break
            self.wiggle(0.7, 2.0, 1)  # side
            if self.tracking_state:
                break
            self.wiggle(-0.7, 2.1, 2)  # down
            if self.tracking_state:
                break

    def add_goal(self, x, y, z):
        self.goals.append([x, y, z])

    def goto_goals(self):
        goals = []
        for goal in self.goals:
            goal_pos = PoseStamped()
            goal_pos.pose.position.x = goal[0]
            goal_pos.pose.position.y = goal[1]
            goal_pos.pose.position.z = goal[2]
            goal_pos.pose.orientation.z = 0.0
            goal_pos.pose.orientation.w = 1.0
            goals.append(goal_pos)
        actionGoal = MoveDroneGoal()
        actionGoal.path.poses = goals
        self.client.wait_for_server()
        self.client.send_goal(actionGoal)

    def send_goal(self, x, y, z):
        goal_pos = PoseStamped()
        goal_pos.pose.position.x = x
        goal_pos.pose.position.y = y
        goal_pos.pose.position.z = z
        goal_pos.pose.orientation.z = 0.0
        goal_pos.pose.orientation.w = 1.0

        goals = [goal_pos]
        actionGoal = MoveDroneGoal()
        actionGoal.path.poses = goals
        self.client.wait_for_server()
        self.client.send_goal(actionGoal)

    def goto_start(self):
        self.send_goal(self.start_pose[0], self.start_pose[1], self.start_pose[2])

    def goto_goal(self):
        self.send_goal(self.goal_pose[0], self.goal_pose[1], self.goal_pose[2])

    def wait_for_result(self):
        self.client.wait_for_result(rospy.Duration.from_sec(20.0))

    def tracking_ok(self):
        return self.tracking_state


if __name__ == '__main__':
    exp = Agent()