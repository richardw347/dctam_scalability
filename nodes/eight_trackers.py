#!/usr/bin/env python

import rospy
import subprocess
import psutil
import sys
from recording_agent import RecordingAgent

GROUND_TRUTH_CONTROL = True
NUM_AGENTS = 1
RECORDING = True


#NAMES = ['arnold', 'bob', 'chappie', 'dave', 'hal', 'murphy', 't1000', 'tars']
NAMES = ['t1000', 'bob', 'chappie', 'dave', 'hal', 'murphy', 't1000', 'tars']

class EightTrackers:
    def __init__(self):
        rospy.init_node("experiment")
        self.agents = []
        self.agent_procs = []
        num_agents = NUM_AGENTS
        for i in range(num_agents):

            agent = NAMES[i]

            proc = subprocess.Popen(['roslaunch dctam_scalability agent_tracker.launch name:=' +
                                     str(agent)], shell=True)
            self.agent_procs.append(proc)

            if RECORDING:
                rec = RecordingAgent(agent)
                self.agents.append(rec)

    def clean_up(self):
        rospy.loginfo("cleaning up")
        for proc in self.agent_procs:
            self.term(proc.pid)

    def term(self, proc_pid):
        process = psutil.Process(proc_pid)
        for proc in process.get_children(recursive=True):
            proc.terminate()
        process.terminate()


if __name__ == '__main__':
    act = EightTrackers()
    rospy.spin()
    act.clean_up()
