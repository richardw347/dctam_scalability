#!/usr/bin/env python
import rospkg
import datetime
import rospy
from std_msgs.msg import Float32
from nav_msgs.msg import Odometry
from dctam_msgs.msg import MoveDroneAction, MoveDroneResult, MoveDroneGoal, Bandwidth
from geometry_msgs.msg import Twist
import actionlib
import tf.transformations
from dctam_msgs.msg import DCTAMTrackerState
from nav_msgs.msg import Path
from sensor_fusion_comm.srv import InitHeight, InitHeightRequest


class RecordingAgent(object):
    def __init__(self, agent):
        self.name = agent
        self.ptam_state = False
        self.tracking_state = False
        self.init = False
        self.truth_z = 0.0
        self.goal_completed = False
        self.last_good_pose = None
        self.goals = []
        self.frontier_points_won = Path()

        self.pose_estimate = None
        self.pose_truth = None
        self.attitude_estimate = None
        self.attitude_truth = None
        self.tracking_time = None
        self.keyframes = 0
        self.mappoints = 0

        self.current_x = 0.0
        self.current_y = 0.0
        self.current_z = 0.0

        self.sent_bw = 0.0
        self.recv_bw = 0.0

        r = rospkg.RosPack()
        self.package_path = r.get_path("dctam_scalability")
        dateStr = datetime.datetime.utcnow().strftime('%Y%m%d_%H%M%S%f')[:-3]
        pose_data_path = self.package_path + "/data/position_error_data_" + agent + "_" + dateStr + ".txt"
        self.pose_data_file = open(pose_data_path, 'a')

        bwsent_data_path = self.package_path + "/data/bandwidth_sent_data_" + agent + "_" + dateStr + ".txt"
        self.bandwidth_sent_file = open(bwsent_data_path, 'a')

        bwrecv_data_path = self.package_path + "/data/bandwidth_recv_data_" + agent + "_" + dateStr + ".txt"
        self.bandwidth_recv_file = open(bwrecv_data_path, 'a')

        self.bw_sent_sub = rospy.Subscriber(str(agent) + "/bandwidth_sent", Bandwidth, self.bw_sent_cb)
        self.bw_recv_sub = rospy.Subscriber(str(agent) + "/bandwidth_recv", Bandwidth, self.bw_recv_cb)
        self.state_sub = rospy.Subscriber(str(agent) + "/ground_truth/state", Odometry, self.state_cb)
        self.tracktime_sub = rospy.Subscriber(str(agent) + "/tracking_time", Float32, self.time_cb)
        self.filter_sub = rospy.Subscriber(str(agent) + "/odom", Odometry, self.filter_state_cb)
        self.ptam_state_sub = rospy.Subscriber(str(agent) + "/state", DCTAMTrackerState, self.ptam_state_cb)
        self.vel_pub = rospy.Publisher(str(agent) + "/cmd_vel", Twist, queue_size=5)
        self.timer = rospy.Timer(rospy.Duration(0.0333), self.timer_callback)

    def init_height(self):
            rospy.wait_for_service(str(self.name) + "/pose_sensor/pose_sensor/initialize_msf_height")
            set_param = rospy.ServiceProxy(str(self.name) + "/pose_sensor/pose_sensor/initialize_msf_height",
                                           InitHeight)
            try:
                rec = InitHeightRequest()
                rec.height = self.truth_z
                rospy.loginfo("init filter with height: " + str(self.truth_z))
                set_param(rec)
            except:
                rospy.logerr("error setting model state")

    def timer_callback(self, event):
        time = rospy.Time.now()
        if self.pose_estimate is not None and self.pose_truth is not None:
            self.pose_data_file.write(str(time) + ";" +
                                      str(self.pose_estimate[0]) + ";" +
                                      str(self.pose_estimate[1]) + ";" +
                                      str(self.pose_estimate[2]) + ";" +
                                      str(self.pose_truth[0]) + ";" +
                                      str(self.pose_truth[1]) + ";" +
                                      str(self.pose_truth[2]) + "\n")
            self.pose_estimate = None
            self.pose_truth = None

    def bw_sent_cb(self, msg):
        assert (isinstance(msg, Bandwidth))
        self.sent_bw += msg.message_size
        self.bandwidth_sent_file.write(str(msg.header.stamp) + ";" +
                                       str(msg.message_size) + "\n")

    def bw_recv_cb(self, msg):
        assert (isinstance(msg, Bandwidth))
        self.recv_bw += msg.message_size
        self.bandwidth_recv_file.write(str(msg.header.stamp) + ";" +
                                       str(msg.message_size) + "\n")

    def time_cb(self, msg):
        self.tracking_time = msg.data

    def ptam_state_cb(self, msg):
        assert (isinstance(msg, DCTAMTrackerState))
        self.keyframes = msg.keyframes
        self.mappoints = msg.mappoints
        self.ptam_state = msg.mapOK
        if msg.trackingQuality in [1, 2]:
            self.tracking_state = True
        else:
            self.tracking_state = False

    def filter_state_cb(self, msg):
        assert (isinstance(msg, Odometry))
        self.current_x = msg.pose.pose.position.x
        self.current_y = msg.pose.pose.position.y
        self.current_z = msg.pose.pose.position.z
        self.attitude_estimate = [msg.pose.pose.orientation.x,
                                  msg.pose.pose.orientation.y,
                                  msg.pose.pose.orientation.z,
                                  msg.pose.pose.orientation.w]
        self.pose_estimate = [msg.pose.pose.position.x,
                              msg.pose.pose.position.y,
                              msg.pose.pose.position.z]

    def state_cb(self, msg):
        assert (isinstance(msg, Odometry))
        self.attitude_truth = [msg.pose.pose.orientation.x,
                               msg.pose.pose.orientation.y,
                               msg.pose.pose.orientation.z,
                               msg.pose.pose.orientation.w]

        self.pose_truth = [msg.pose.pose.position.x,
                           msg.pose.pose.position.y,
                           msg.pose.pose.position.z]
        if self.tracking_state:
            self.last_good_pose = [msg.pose.pose.position.x,
                                   msg.pose.pose.position.y,
                                   msg.pose.pose.position.z]