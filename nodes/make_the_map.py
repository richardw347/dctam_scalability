#!/usr/bin/env python

import rospy
from geometry_msgs.msg import PoseArray, PoseStamped
from nav_msgs.msg import Odometry
from dctam_msgs.msg import MoveDroneAction, MoveDroneResult, MoveDroneGoal
from dctam_msgs.srv import GetPoints, GetPointsRequest, GetPointsResponse
from sensor_msgs.msg import PointCloud
import actionlib
from dctam_msgs.msg import DCTAMTrackerState
from geometry_msgs.msg import Twist
from gazebo_msgs.srv import SetModelState, SetModelStateRequest, SetModelStateResponse
from dynamic_reconfigure.msg import BoolParameter
from dynamic_reconfigure.srv import Reconfigure, ReconfigureRequest, ReconfigureResponse
from sensor_fusion_comm.srv import InitHeight, InitHeightRequest
from nav_msgs.msg import Path
import numpy as np
import subprocess
import psutil
import math
import sys
import copy
from random import randint
import matplotlib.pyplot as plt
from experiment_agent import Agent

GROUND_TRUTH_CONTROL = True
NUM_AGENTS = 20
HEIGHT = 2.2
RADIUS = 4.0

class Experiment:
    def __init__(self):
        rospy.init_node("experiment")
        self.goals = []
        self.client = actionlib.SimpleActionClient('agent/move_drone', MoveDroneAction)
        anuglar_range = math.pi * 2
        self.goals.append([0,0,HEIGHT])
        for i in range(20):
            agent_num = i
            x = math.cos(agent_num * anuglar_range / NUM_AGENTS) * RADIUS
            y = math.sin(agent_num * anuglar_range / NUM_AGENTS) * RADIUS
            self.goals.append([x, y, HEIGHT])

        rospy.sleep(2.0)

    def send_goal(self, x, y, z):
        goal_pos = PoseStamped()
        goal_pos.pose.position.x = x
        goal_pos.pose.position.y = y
        goal_pos.pose.position.z = z
        goal_pos.pose.orientation.z = 0.0
        goal_pos.pose.orientation.w = 1.0

        goals = [goal_pos]
        actionGoal = MoveDroneGoal()
        actionGoal.path.poses = goals
        self.client.wait_for_server()
        self.client.send_goal(actionGoal)

    def wait_for_result(self):
        self.client.wait_for_result(rospy.Duration.from_sec(20.0))


    def spin(self):
        rospy.loginfo("building map")
        for goal in self.goals:
            self.send_goal(goal[0], goal[1], goal[2])
            self.wait_for_result()
        rospy.loginfo("map done")

if __name__ == '__main__':
    act = Experiment()
    act.spin()
    rospy.spin()