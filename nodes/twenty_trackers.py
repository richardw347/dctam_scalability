#!/usr/bin/env python

import rospy
import subprocess
import psutil
import sys
from recording_agent import RecordingAgent

GROUND_TRUTH_CONTROL = True
NUM_AGENTS = 20
HEIGHT = 1.9
RADIUS = 3.0
QUADRANT = 3
USE_QUADS = True
RECORDING = True


class TwentyTrackers:
    def __init__(self, quadrant):
        rospy.init_node("experiment2")
        self.agents = []
        self.agent_procs = []
        if USE_QUADS:
            num_agents = NUM_AGENTS / 5
        else:
            num_agents = NUM_AGENTS

        for i in range(10, NUM_AGENTS):
            if USE_QUADS:
                agent = "agent" + str(i + (quadrant * num_agents))
            else:
                agent = "agent" + str(i)

            proc = subprocess.Popen(['roslaunch dctam_scalability agent_tracker.launch name:=' +
                                     str(agent)], shell=True)
            self.agent_procs.append(proc)

            if RECORDING:
                rec = RecordingAgent(agent)
                self.agents.append(rec)

    def clean_up(self):
        rospy.loginfo("cleaning up")
        for proc in self.agent_procs:
            self.term(proc.pid)

    def term(self, proc_pid):
        process = psutil.Process(proc_pid)
        for proc in process.get_children(recursive=True):
            proc.terminate()
        process.terminate()


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("usage: twenty_trackers.py quadrant")
    else:
        act = TwentyTrackers(int(sys.argv[1]))
        rospy.spin()
        act.clean_up()
