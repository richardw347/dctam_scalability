#!/usr/bin/env python
import rospy
import rospkg
import datetime
import math
import numpy as np
import tf.transformations
from nav_msgs.msg import Odometry
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d
import os


def open_file_and_process(directory, file):
    #rospy.loginfo("Processing file: %s"%file)
    date_str = file.strip("position_error_data_").strip(".txt")
    f = open(directory+file, 'r')
    lines = f.readlines()
    time = []
    est_x = []
    est_y = []
    est_z = []
    tru_x = []
    tru_y = []
    tru_z = []
    first_time = 0.0
    first = True
    #print len(lines)
    for line in lines:
        data = line.split(";")
        if first:
            first_time = float(data[0])
            first = False
        time.append(float(data[0]) - first_time)
        est_x.append(float(data[1]))
        est_y.append(float(data[2]))
        est_z.append(float(data[3]))
        tru_x.append(float(data[4]))
        tru_y.append(float(data[5]))
        tru_z.append(float(data[6]))

    # x_offset = tru_x[0] - est_x[0]
    # y_offset = tru_y[1] - est_y[1]
    # z_offset = tru_z[2] - est_z[2]

    # for idx in range(len(tru_x)):
    #    tru_x[idx] -= (x_offset*2)
    #    tru_y[idx] -= y_offset
    #    tru_z[idx] -= z_offset

    x_scale = 1.0
    y_scale = 1.0
    z_scale = 1.18
    # z_scale = compute_scale_factor(est_z, tru_z)
    #
    # rospy.loginfo("Scale factors set")
    # rospy.loginfo("Values are x: %f y: %f z: %f"%(x_scale, y_scale, z_scale))
    #
    for idx in range(len(est_x)):
         est_x[idx] *= x_scale
         est_y[idx] *= y_scale
         est_z[idx] *= z_scale
         #est_y[idx] -= 0.7
         #est_x[idx] -= 0.05

    mean_x, std_dev_x, rmse_x = compute_rmse_error(est_x, tru_x)
    mean_y, std_dev_y, rmse_y = compute_rmse_error(est_y, tru_y)
    mean_z, std_dev_z, rmse_z = compute_rmse_error(est_z, tru_z)

    mean_error = (mean_x + mean_y + mean_z) / 3.0
    std_deviation = (std_dev_x + std_dev_y + std_dev_z) / 3.0
    rmse = (rmse_x + rmse_y + rmse_z) / 3.0

    #rmse_over_time = compute_rmse_over_time(est_x, est_y, est_z, tru_x, tru_y, tru_z)

    #rospy.loginfo("Overall Mean error: %f"%mean_error)
    #rospy.loginfo("Overall Standard deviation: %f"%std_deviation)
    #rospy.loginfo("Overall Route Mean Squared Error: %f"%rmse)
    #rospy.loginfo("===================================\n")
    print rmse
    # fig = plt.figure(figsize=(8, 4), dpi=100)
    # plt.plot(time, rmse_over_time, label='RMSE', color='Red')
    # plt.xlabel('Time in minutes')
    # plt.ylabel('Route Mean Squared Error')
    # plt.legend()
    # file = directory + "rmse_growth_plot_" + date_str + ".png"
    # plt.savefig(file, bbox_inches='tight')
    #
    # fig = plt.figure(figsize=(8, 4), dpi=100)
    # plt.plot(est_x, est_y, label="CTAM Estimate", color='Red')
    # plt.plot(tru_x, tru_y, label="Ground Truth", color='Green')
    # plt.xlabel('X Position in Metres')
    # plt.ylabel('Y Position in Metres')
    # plt.legend()
    # file = directory + "position_error_plot_" + date_str + ".png"
    # plt.savefig(file, bbox_inches='tight')
    #
    # fig = plt.figure()
    # ax = fig.add_subplot(111, projection='3d')
    # ax.plot_wireframe(est_x, est_y, est_z, color="Red", label="CTAM Estimate")
    # ax.plot_wireframe(tru_x, tru_y, tru_z, color="Green", label="Ground Truth")
    # ax.legend()
    # ax.set_xlabel('X Position in Metres')
    # ax.set_ylabel('Y Position in Metres')
    # ax.set_zlabel('Z Position in Metres')
    # ax.set_zlim(0.0, 3.0)
    # file = directory + "position_error_plot_" + date_str + "_3d.png"
    # plt.savefig(file, bbox_inches='tight')


def compute_rmse_error(estimated, truth):
    total_error = 0
    total_error_squared = 0
    error_samples = []
    for idx in range(len(estimated)):
        error = truth[idx] - estimated[idx]
        total_error += error
        total_error_squared += (error * error)
        error_samples.append(error)

    mean_error = total_error / len(estimated)
    std_dev = np.std(error_samples)
    mean_error_sq = total_error_squared / len(estimated)
    rmse = math.sqrt(mean_error_sq)

    #rospy.loginfo("Mean error: %f"%mean_error)
    #rospy.loginfo("Standard deviation: %f"%std_dev)
    #rospy.loginfo("Route Mean Squared Error: %f"%rmse)
    #rospy.loginfo("===================================\n")

    return mean_error, std_dev, rmse


def compute_rmse_over_time(est_x, est_y, est_z, tru_x, tru_y, tru_z):
    rmse_all = []
    prev_rmse = 0.0
    first = True
    for idx in range(len(est_x)):
        error_x_sq = math.pow((tru_x[idx] - est_x[idx]), 2)
        error_y_sq = math.pow((tru_y[idx] - est_y[idx]), 2)
        error_z_sq = math.pow((tru_z[idx] - est_z[idx]), 2)
        total_error_sq = (error_x_sq + error_y_sq + error_z_sq) / 3.0
        rmse = math.sqrt(total_error_sq)
        if first:
            rmse_all.append(rmse)
            first = False
        else:
            rmse = (0.2 * rmse) + (0.8 * prev_rmse)
            rmse_all.append(rmse)
    return rmse_all


def compute_scale_factor(estimated, truth):
    scale = []
    samples = 0
    for idx, estimate in enumerate(estimated):
        if idx == 0 or idx == (len(estimated) - 1):
            continue
        diff_est = estimated[idx] - estimated[idx - 1]
        diff_tru = truth[idx] - truth[idx - 1]
        #print diff_est, diff_tru
        if abs(diff_est) > 0 and abs(diff_tru) > 0:
            scale.append(diff_tru / diff_est)
            samples += 1
    if samples > 0:
        scale = np.median(np.array(scale))
    else:
        scale = 1.0

    return scale

if __name__ == '__main__':
    rospy.init_node("position_error_plot")

    r = rospkg.RosPack()
    package_path = r.get_path("dctam_scalability")
    data_path_pose = package_path + "/data/"
    files = os.listdir(data_path_pose)
    for file in files:
        if file.startswith("position_error_data_"):
            open_file_and_process(data_path_pose, file)